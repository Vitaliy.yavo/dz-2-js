/* Відповідь 1:  
1.Number - для будь-яких чисел: цілих або чисел з плаваючою точкою.
2.BigInt - для гігантських чисел, більше 2^53 і менше -2^53.
3.string - для строк.
4.Boolean - логічний тип. Результат true/false (так/ні).
5.null - спеціальне значення, яке означає "пусто", "значення невідоме".
6.undefined - значення не було присвоєно.
7.object - для складних структур даних.
8.symbol - для унікальних ідентифікаторів.*/

/* Відповідь 2: 
У JavaScript оператори == (рівність) і === (строга рівність) використовуються для порівняння значень.
Їхня різниця полягає в тому, як вони обробляють типи даних при порівнянні.
Оператор === повертає true тільки тоді, коли значення рівні і типи даних також співпадають. 
Наприклад, 1 === '1' буде повертати false, оскільки значення різні типи (число та рядок).
А при == повертав би при цій же умові true.
Рекомендується використовувати === (строгу рівність), коли необхідно порівняти як значення, так і типи даних,
для уникнення неочікуваної поведінки, пов'язаної з неявним перетворенням типів.*/


/* Відповідь 3: 
Оператор - це символ або символьна послідовність в програмі,
яка виконує певну операцію над одним чи кількома операндами.
Оператори визначають, які дії необхідно виконати над операндами, щоб отримати бажаний результат.
Основні опаротори:
1.Арифметичні оператори: +, -, *, / ;
2.Логічні оператори: &&, ||, ! ;
3.Порівняння операторів: == , != , > , < ;
4.Присвоювальні оператори: =, +=, -= ;
5.Умовні оператори: if, else, switch ;
6.Тернарний оператор: ? : ; */

let name = prompt("Введіть ваше ім'я:");
let age;

while (!name || isNaN(age) || age <= 0) {
  name = prompt("Введіть ваше ім'я:");
  age = parseInt(prompt("Введіть ваш вік:"));
}

if (age < 18) {
  alert("You are not allowed to visit this website.");
} else if (age >= 18 && age <= 22) {
  const confirmResult = confirm("Are you sure you want to continue?");
  if (confirmResult) {
    alert(`Welcome, ${name}!`);
  } else {
    alert("You are not allowed to visit this website.");
  }
} else {
  alert(`Welcome, ${name}!`);
}






